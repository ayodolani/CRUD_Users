﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace profile
{
    public partial class deleteUser : Form
    {
        database mydb;
        public deleteUser()
        {
            InitializeComponent();

            mydb = new database();
        }

        private void textBoxDeleteUserId_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void deleteUser_Load(object sender, EventArgs e)
        {
            mydb.openconnection();
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBoxDeleteUserId.Text);

            mydb.deleteUser(id);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 isForm1 = new Form1();
            isForm1.Show();
            this.Hide();
        }
    }
}
