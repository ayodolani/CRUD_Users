﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace profile
{
    class database
    {
        string con;
        DataTable dTable = new DataTable();

        MySqlCommand mycommand;
        MySqlConnection myconnect;
        public database()
        {
            con = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=admin";
            myconnect = new MySqlConnection(con);
            mycommand = new MySqlCommand();
        }

        public void openconnection()
        {
            try
            {
                myconnect.Open();
                
            }
            catch (MySqlException errors)
            {
                MessageBox.Show(errors.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void insertNewUser(string firstname, string lastname, string password, string email)
        {
            myconnect.Open();
            try
            {
                string query = "insert into users(firstname, lastname, password, email) values (@firstname,@lastname,@password,@email)";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@firstname", firstname);
                mycommand.Parameters.AddWithValue("@lastname", lastname);
                mycommand.Parameters.AddWithValue("@password", password);
                mycommand.Parameters.AddWithValue("@email", email);

                mycommand.ExecuteNonQuery();
                MessageBox.Show("New User Inserted Succesfully");
            }
            catch (MySqlException errors)
            {
                MessageBox.Show(errors.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void viewUser()
        {
            myconnect.Open();
            try
            {
                string query = "select * from users";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
               // MySqlDataAdapter myAdapter = new MySqlDataAdapter();
                mycommand.ExecuteNonQuery();
               // myAdapter.SelectCommand = mycommand;
                //myAdapter.Fill(dTable);
                //DataGrid myGrid = Application.OpenForms["viewUsers"].Controls["dataGridView1"] as DataGrid;
                //zzmyGrid.DataSource = dTable;


                MySqlDataReader myreader = mycommand.ExecuteReader();
                while (myreader.Read())
                {
                    
                    Console.WriteLine(myreader.GetString("lastname"));
                    Console.WriteLine(myreader.GetString("firstname"));
                    Console.WriteLine(myreader.GetString("lastname"));
                    Console.WriteLine(myreader.GetString("email"));
                    
                }
                MessageBox.Show("Query Successful");
            }
            catch (MySqlException errors)
            {
                MessageBox.Show(errors.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void deleteUser(int id)
        {
            myconnect.Open();
            try
            {
                string query = "delete from users where id = @id";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@id", id);
                mycommand.ExecuteNonQuery();
                MessageBox.Show("Record Deleted Succesfully");
            }
            catch (MySqlException errors)
            {
                MessageBox.Show(errors.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }

        public void upDateUser(int id, string firstname, string lastname, string password, string email)
        {
            myconnect.Open();
            try
            {
                string query = "update users set firstname=@firstname,lastname=@lastname,password=@password,email=@email where id=@id" ;
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.Parameters.AddWithValue("@id", id);
                mycommand.Parameters.AddWithValue("@firstname", firstname);
                mycommand.Parameters.AddWithValue("@lastname", lastname);
                mycommand.Parameters.AddWithValue("@password", password);
                mycommand.Parameters.AddWithValue("@email", email);
                mycommand.ExecuteNonQuery();
                MessageBox.Show("Record Updated Succesfully");
            }
            catch (MySqlException errors)
            {
                MessageBox.Show(errors.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }
    }
}
